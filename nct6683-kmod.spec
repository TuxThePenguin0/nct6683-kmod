%if 0%{?fedora}
%global buildforkernels akmod
%global debug_package %{nil}
%endif

%global prjname nct6683
%global commit d099af5b1a4e74a696e63ce6ce8b30b53fe1d752

Name:           %{prjname}-kmod
Version:        6.7
Release:        1%{?dist}
Summary:        Kernel module (kmod) for NCT6683
License:        GPL-2.0-only
URL:            https://gitlab.com/TuxThePenguin0/nct6683
Source:         https://gitlab.com/TuxThePenguin0/nct6683/-/archive/%{commit}/nct6683-%{commit}.tar.bz2

BuildRequires:  gcc
BuildRequires:  make
BuildRequires:  %{_bindir}/kmodtool

Provides:       %{name}-common = %{version}-%{release}

# kmodtool does its magic here
%{expand:%(kmodtool --target %{_target_cpu} --kmodname %{prjname} %{?buildforkernels:--%{buildforkernels}} %{?kernels:--for-kernels "%{?kernels}"} 2>/dev/null) }

%description
Upstream version of the NCT6683 Super I/O chip driver, modified to support
extra control features.

%prep
# error out if there was something wrong with kmodtool
%{?kmodtool_check}

# print kmodtool output for debugging purposes:
kmodtool --target %{_target_cpu} --kmodname %{prjname} %{?buildforkernels:--%{buildforkernels}} %{?kernels:--for-kernels "%{?kernels}"} 2>/dev/null

%setup -q -c %{prjname}-%{commit}

for kernel_version in %{?kernel_versions} ; do
    cp -a %{prjname}-%{commit} _kmod_build_${kernel_version%%___*}
done

%build
for kernel_version in %{?kernel_versions}; do
    make %{?_smp_mflags} -C "${kernel_version##*___}" M=${PWD}/_kmod_build_${kernel_version%%___*} modules
done

%install
for kernel_version in %{?kernel_versions}; do
    install -D -m 755 _kmod_build_${kernel_version%%___*}/nct6683.ko  ${RPM_BUILD_ROOT}%{kmodinstdir_prefix}/${kernel_version%%___*}/%{kmodinstdir_postfix}/nct6683.ko
done
%{?akmod_install}

%clean
rm -rf ${RPM_BUILD_ROOT}

%package common
Summary: Dummy package

%description common

%files common

%changelog
* Mon Dec 4 2023 Alexander Warnecke <awarnecke002@hotmail.com> - 6.7-1
- Initial version
